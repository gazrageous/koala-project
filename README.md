# Koala Take Home Weather Project

## Local Execution 

```
    netlify dev
```

With the inclusion of netlify functions `npm start` will need to be replaced with `netlify dev` for spinning up a local dev environment to ensure all features are enabled. Request access to netlify from me at `garethwyndhamjones@gmail.com` and then authenticate through the netlify cli `npm i netlify-cli -g` if desired. 

Or visit the hosted site at `wombat-weather.netlify.app` to see the source code in production. 


## Areas to Improve / Considerations

* Once key callout is requesting for user location on load without context. This is flagged both by the browser and is also not best practice from a UX perspective. Could be improved by assigning to a user interaction with a button or in app UI modal. 
* Consistent entrance / exit animations would have been a nice to have for each of the weather tiles when each new location request was made and returned.  

* Chiefly, i believe a better seperation of concerns between client vs server could have been achieved in this build. Local JSON is suited to speed, but requires more handling in the associated components than is ideal. Data should be better treated within the serverless functions perhaps before being supplied to components as props / context provders.

* As above, there is a consideration that metric conversion could be done client side instead of pinging the API for new data, but this also factors into the above dilemma.

* An argument for larger server-side architecture can be made when considering larger city Databases. My intial city list was significantly larger for the search recommendation but size made it difficult to support on the client. However, having remote storage will then require new API reqs for search recommendations as well as follow on weather requests and cross-referencing.  

## Docs

```
    Layout.jsx
```
Layout is primarily concerned with organising the page structure and component tree, as well as controlling majority of the theme styling and related controls for the user. The Component tree is quite small consisting of only:

```
| Layout 
|--------| LocationSearch
|--------| WeatherTiles
```

```
    LocationSearch.jsx
```
Provides text input for user to search for a city by name value. filters `data/city-list.min.json` for appropriate name and coord values to make request on enter. Provides live recommendations on input change,  click event on recommendations also fires appropriately assigned data as argument to `getLocation`. Radio field controls units of measurement, change makes new `getLocation` request with previously stored `location` state.


```
    WeatherTile.jsx
```
Displays response data from `getLocation` for the user, requires array `forecast` on `location`. Scoped functions are mostly for handling appropriate UNIX datetime values, and for serving appropriate icons in each tile for `weather` value. Also assigns click and touch / swipe events for user navigation to explore provided data. Has conditionally rendered message if no available locaiton. This component is also responsible for requesting initial `navigator.geolocation` value.


```
    ThemeContext.js
```
Controls a Boolean state that communcates dark vs light theme, can be subscribed to by any component in the tree to adjust UI elements appropriately - mostly leveraged by Layout - as above. 


```
    LocationContext.js
```
Provides app wide information on currently selected locaiton (stored in state), and exposes a centralised method to request new location. Requesting new location will send request and then cross-reference response with `data/city-list.min.json` to ensure semantic city names are added (as opposed to OpenWeather's timezone values) These functions for comparing coords and names are also provided for subscribed components. 

```
    req-weather.js
```
Makes the OpenWeatherAPI request for the client, and then formats appropriately for the location state object. Reads OPENWEATHER_API key from netlify env vars. Accepts all OpenWeather query params as its own query string - could be scaled as a larger piece of middleware for the application.



## Brief

The purpose of this test is for us to get a sense of how you would approach designing and implementing a JavaScript/CSS/HTML weather widget. Applicants are encouraged to write code for this test as for a production grade application.

There's more than one way to skin a cat, therefore feel free to list down your assumptions as you go along (e.g. in code comments).

Although not required, you can use any js framework you like. To showcase your front-end skills as part of this test, you may want to avoid using UI libraries.

### Requirements

Based on the wireframe (weather-widget.jpg), create a “weather widget” app that:

1. Shows your location's current and future weather data.
2. Able to search a specific location weather data.
3. Has a dark / light mode.
4. Adheres to minimum AA accessibility guidelines.

Wireframes are intentionally left plain for maximum creativity! A delightful use of animations is a plus.

##### Widget

The widget should read the end user’s current location using `navigator.geolocation` (or the entered location), and retrieve the current weather conditions for that location using the [Open Weather Map API](https://openweathermap.org/api). Defaulted to the current time, users should be able to navigate between the days. (see image included)

### Deliverables

There is no strict time limit (unless agreed upon) but most people get back to us within a few days. Source code for the solution described above: share your repository, upload it as an accessible link or forward a zip file of the source code.
