require('dotenv').config();
const fetch = require('node-fetch'); 

const API_ENDPOINT = "https://api.openweathermap.org/data/2.5/onecall?";
const OPENWEATHER_API_KEY = process.env.OPENWEATHER_API_KEY;

const sanitise = (data, units, name) => {
  let clean = data.daily.map(day => { 
    return {
      dt: day.dt,
      temperature: Math.floor(day.temp.day),
      weather: day.weather[0].main
    }
  })

  return JSON.stringify({ 
    units,
    name,
    lat: data.lat,
    lon: data.lon,
    forecast: clean
  });
}

exports.handler = async (event) => {
  const { lat, lon, units, name } = event.queryStringParameters

  let raw = await fetch(`${API_ENDPOINT}lat=${lat}&lon=${lon}&exclude=minutely,hourly&units=${units}&appid=${OPENWEATHER_API_KEY}`, { headers: { "Accept": "application/json" } })
    .then(response => response.json())
    .then(data => {
      return data
    })
    .catch(error => ({ statusCode: 422, body: String(error) }));
    
    return { statusCode: 200, body: sanitise(raw, units, name)};
};
