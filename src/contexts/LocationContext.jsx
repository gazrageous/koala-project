import React, { createContext, useState, useEffect } from 'react';
import cities from '../data/city-list.min.json'

export const LocationContext = createContext();

const LocationContextProvider = (props) => {

    const [location, setLocation] = useState({
        name: '',
        units: 'metric',
        lat: '-30',
        lon: '150',
        forecast: []
    });

    const getLocation = async (newLocation) => {
        let { lat, lon, units, name } = newLocation;
        try {
            fetch(`/api/req-weather?lat=${lat}&lon=${lon}&units=${units}&name=${name}`)
                .then(response => response.json())
                .then(data => {
                    let cityName = matchCityCoordinates(data.lat, data.lon);
                    setLocation({ ...data, name: cityName});
                    localStorage.setItem('userLocation', JSON.stringify(location))
                })
                .catch(error => ({ statusCode: 422, body: String(error) }));
        } catch (error) {
            console.error(error);
        }
    };

    const matchCityName = (input) => {
        return cities.filter(city => city.name.toLowerCase().startsWith(input.toLowerCase()));
    }

    const inRange = (x, min, max) => {
        return ((x-min)*(x-max) <= 0);
    }

    const matchCityCoordinates = (lat, lon) => {
        return cities.filter(city => 
            inRange(lat, parseFloat(city.lat.toFixed(2), 10) - 0.1, parseFloat(city.lat.toFixed(2), 10) + 0.1) && 
            inRange(lon, parseFloat(city.lon.toFixed(2), 10) - 0.1, parseFloat(city.lon.toFixed(2), 10) + 0.1) 
        )[0].name
    }

    useEffect(() => {
        let prev = localStorage.getItem('userLocation')
        if (prev) setLocation(JSON.parse(prev))
    }, [])


    return (
        <LocationContext.Provider value={{ location, setLocation, getLocation, matchCityName, matchCityCoordinates }}>
            {props.children}
        </LocationContext.Provider>
    );
}

export default LocationContextProvider;