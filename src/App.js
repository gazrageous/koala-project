import Layout from './components/Layout'
import ThemeContextProvider from './contexts/ThemeContext';
import LocationContextProvider from './contexts/LocationContext';

function App() {
    return (
        <div className="App">
          <ThemeContextProvider>
              <LocationContextProvider>
                  <Layout/>
              </LocationContextProvider>
            </ThemeContextProvider>
        </div>
    );
}

export default App;
