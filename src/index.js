import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

console.log(
  "%cHello There!",
  "color:#007e8f;font-family:system-ui;font-size:1.5rem;font-weight:bold"
);
console.log(
  "%cThis is an application built by Gareth Jones as part of a technical assessment at Koala, enjoy!",
  "color:#eef8f6;font-family:system-ui;font-size:1rem;font-weight:bold"
)

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);