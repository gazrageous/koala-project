import { useState } from 'react'

export default async function useOnce (fn) {
    const [ hasRun, setHasRun ] = useState(false);
  
    if (!hasRun) {
      await fn();
      setHasRun(true);
    }
}
  
