import React, { useContext } from 'react'
import LocationSearch from './LocationSearch'
import WeatherTiles from './WeatherTiles'

import { ThemeContext } from '../contexts/ThemeContext';

import '../styles/Layout.scss'

export default function Layout() {
    
    const { isDarkTheme, toggleTheme } = useContext(ThemeContext);

    return (
        <div className={ isDarkTheme ? 'layout layout-dark' : 'layout layout-light'}>
            <div className="container">
                <div className="layout-inner">
                    <header>
                        <label htmlFor="theme-toggle">toggle theme</label>
                        <button id="theme-toggle" className="btn btn-themetoggle" aria-label="theme-toggle" onClick={() => toggleTheme()}><i className={ isDarkTheme ? 'uil uil-sun' : 'uil uil-moon'}></i></button>
                    </header>
                    <nav>
                        <h1>wombat <span className="text-splash">weather</span></h1>
                        <LocationSearch/>
                    </nav>
                    <main>
                        <WeatherTiles/>
                    </main>
                    <footer>
                        <p>created by <a href="https://gitlab.com/gazrageous" target="_blank" rel="noreferrer noopener" >gareth jones</a> for the team at <span className="text-splash">koala</span></p>
                    </footer>
                </div>
            </div>
        </div>
    )
}
