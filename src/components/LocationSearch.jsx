import React, { useState, useContext } from 'react'
import { ThemeContext } from '../contexts/ThemeContext';
import { LocationContext } from '../contexts/LocationContext';

import '../styles/LocationSearch.scss'

export default function LocationSearch() {
        
    const { location, getLocation, matchCityName } = useContext(LocationContext);
    const { isDarkTheme } = useContext(ThemeContext);

    const [ suggested, setSuggested ] = useState([])

    const submitLocation = (revised) => {

        let { name } = revised
        setSuggested([])
        document.querySelector(".input-search").value = name;
        getLocation({
            ...location, 
            name, 
            lat: revised.lat,
            lon: revised.lon
        })
    }

    const updateRecommendations = (input, event) => {
        if (input === "") setSuggested([])

        if (input.length < 2) return
        let matchingLocations = matchCityName(input)
        if (event.keyCode === 13) submitLocation(matchingLocations[0])
        else {
            setSuggested(matchingLocations) 
        }
    }

    return (
        <div className={ isDarkTheme ? 'search search-dark' : ' search search-light'}>
            <label className="input-search_label" htmlFor="city-search">City search:</label>
            <input id="city-search" onKeyUp={(e) => updateRecommendations(e.target.value, e)} className="input-search"></input>
            {
                suggested.length > 0 ? 
                    <ul className="search-reclist">
                        {
                            suggested.map((option, i) => (
                                <li onClick={() => submitLocation(option)} key={i}>{option.name}</li>
                            ))
                        }
                    </ul> : ""
            }
            <div className="radio-container">
                <input className="checkbox-tools" type="radio" defaultChecked name="units" id="tool-3"/>
                <label onClick={()=> getLocation({...location, units: 'metric'})} className="for-checkbox-tools" htmlFor="tool-3">°C</label>
                
                <input className="checkbox-tools" type="radio" name="units" id="tool-4"/>
                <label onClick={()=> getLocation({...location, units: 'imperial'})} className="for-checkbox-tools" htmlFor="tool-4">°F</label>
            </div>
        </div>
    )
}
