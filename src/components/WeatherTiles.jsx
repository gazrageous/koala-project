import React, { useContext, useState } from 'react'
import useOnce from '../hooks/useOnce'
import { ThemeContext } from '../contexts/ThemeContext';
import { LocationContext } from '../contexts/LocationContext';

import '../styles/WeatherTiles.scss'

export default function WeatherTiles() {

    const { location, getLocation } = useContext(LocationContext);
    const { isDarkTheme } = useContext(ThemeContext);

    const [activeTile, setActiveTile] = useState(0)

    const weatherIcons = [
        {
            weather: "Clear",
            icon: "sun"
        },
        {
            weather: "Clouds",
            icon: "clouds"
        },
        {
            weather: "Rain",
            icon: "cloud-showers-heavy"
        },
        {
            weather: "Snow",
            icon: "snowflake"
        },
        {
            weather: "Thunderstorm",
            icon: "thunderstorm"
        },
        {
            weather: "Drizzle",
            icon: "cloud-drizzle"
        },
    ]

    let lastTouchVal;

    const getIcon = (weatherString) => {
        let result = weatherIcons.find(x => x.weather === weatherString)
        return String(result.icon)
    }
    
    const getDayOfWeek = (unixdt) => {
        let a = new Date(unixdt * 1000);
        let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
        return days[a.getDay()]
    }

    const getOrdinalNum =(n) => {
        return n + (n > 0 ? ['th', 'st', 'nd', 'rd'][(n > 3 && n < 21) || n % 10 > 3 ? 0 : n % 10] : '');
    }

    const getDisplayedDate = (unixdt) => {
        let a = new Date(unixdt * 1000);
        let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        let month = months[a.getMonth()];
        let date = a.getDate();
        return month + ' ' + getOrdinalNum(date);
    }

    useOnce(() => {
        if('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                getLocation({ ...location, lat: position.coords.latitude, lon: position.coords.longitude })
            })
        }
    })

    return (
        location && location.forecast ?
            <ul className={ isDarkTheme ? "weather-tile-container dark" : "weather-tile-container light"}>
                <li className={ activeTile === 0 ? "weather-tile bookend" : "weather-tile bookend hide" }></li>
                {
                    location.forecast.map((day, i) => (
                        <li 
                            className={ 
                                i === activeTile ? "weather-tile current" : 
                                i - activeTile === -1 ? "weather-tile prev" :
                                i - activeTile === 1 ? "weather-tile next" :
                                "weather-tile hide" 
                            } 
                            key={i}
                            onClick={ () => {
                                if (i !== activeTile && i < activeTile) {
                                    setActiveTile(activeTile - 1)
                                } else if (i !== activeTile && i > activeTile) {
                                    setActiveTile(activeTile + 1)
                                }
                            }}
                            onTouchStart={ (e) => lastTouchVal = e.changedTouches[0].clientX}
                            onTouchEnd={ (e) => {
                                console.log(i)
                                console.log(location.forecast.length)
                                if (e.changedTouches[0].clientX < lastTouchVal && i !== location.forecast.length - 1) {
                                    setActiveTile(activeTile + 1)
                                } else if (e.changedTouches[0].clientX  > lastTouchVal && i !== 0) {
                                    setActiveTile(activeTile - 1)
                                }
                            }}
                        >
                            <h2>{ getDayOfWeek(day.dt) }</h2>
                            <p className="date">{ getDisplayedDate(day.dt) }</p>
                            <p className="weather">{ day.weather }</p>
                            <div className="temperature-container">
                                <i className={`uil uil-${getIcon(day.weather)}`}></i>
                                <p>{day.temperature}{ location.units === 'metric' ? '°C' : '°F'}</p>
                            </div>
                            <p className="city-stamp">{ location.name }</p>
                        </li>
                    ))
                }
                <li className={ activeTile === location.forecast.length - 1 ? "weather-tile bookend" : "weather-tile bookend hide" }></li>
            </ul> :
            <div className="no-loc-container">
                <i className="uil uil-location-point"></i>
                <p>We're trying to find your location!</p>
                <p><span className="text-primary">Enable location services</span> on your device or <span className="text-primary">use the search bar above </span>to find your city.</p>
            </div>
    )
}
